const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth.js");

// Check User
router.post("/check", (req, res) => {
  userController
    .checkUser(req.body)
    .then((resultFromController) => res.send(resultFromController));
});
// User Registration
router.post("/register", (req, res) => {
  userController
    .registerUser(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// User Login
router.post("/login", (req, res) => {
  userController
    .loginUser(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// Get all user details (Admin Only)
router.get("/all", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    userController
      .getAllUserDetails()
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({
      isSuccess: false,
      reason: "unauthorized",
    });
  }
});

// Get user Profile
router.get("/profile", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  userController
    .getUserProfile(userData.id)
    .then((resultFromController) => res.send(resultFromController));
});
router.put("/update", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  userController
    .updateUserProfile(userData.id, req.body)
    .then((resultFromController) => res.send(resultFromController));
});

router.get("/profile/upload", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  userController
    .uploadImageProfile(userData.id, req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// Set User As Admin (Admin Only)
router.put("/:userId/promote", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    userController
      .userAsAdmin(req.params.userId, req.body)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({
      isSuccess: false,
      reason: "unauthorized",
    });
  }
});

router.put("/resetPassword", (req, res) => {
  auth.userResetPassword(req.body).then((result) => res.send(result));
});

router.put("/forgotPassword", (req, res) => {
  auth.userForgotPassword(req.body).then((result) => res.send(result));
});

router.put("/changePassword", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  userController
    .userChangePassword(userData.id, req.body)
    .then((resultFromController) => res.send(resultFromController));
});

router.put("/order/remove/:productId/:qty", auth.verify, (req, res) => {
  authData = auth.decode(req.headers.authorization);
  if (!authData.isAdmin) {
    userController
      .userSinglePendingOrderRemove(
        authData.id,
        req.params.qty,
        req.params.productId
      )
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({
      isSuccess: false,
      reason: "admin user",
    });
  }
});

// Non-Admin User Checkout (Create Order)
router.put("/order/:productId/:qty", auth.verify, (req, res) => {
  authData = auth.decode(req.headers.authorization);
  if (!authData.isAdmin) {
    userController
      .userOrder(req.params.productId, req.params.qty, authData.id)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({
      isSuccess: false,
      reason: "admin user",
    });
  }
});

router.put("/checkout", auth.verify, (req, res) => {
  authData = auth.decode(req.headers.authorization);
  if (!authData.isAdmin) {
    userController
      .userCheckout(authData.id)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({
      isSuccess: false,
      reason: "admin user",
    });
  }
});

// Retrieve authenticated user's orders
// Pending order
router.get("/order/pending", auth.verify, (req, res) => {
  authData = auth.decode(req.headers.authorization);
  userController
    .userGetPendingOrder(authData)
    .then((resultFromController) => res.send(resultFromController));
});
// Purchased order
router.get("/order/purchased", auth.verify, (req, res) => {
  authData = auth.decode(req.headers.authorization);
  userController
    .userGetPurchasedOrder(authData)
    .then((resultFromController) => res.send(resultFromController));
});

// additional multiple order
router.put("/order", auth.verify, (req, res) => {
  authData = auth.decode(req.headers.authorization);
  if (!authData.isAdmin) {
    userController
      .userMultipleOrder(authData.id, req.body)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({
      isSuccess: false,
      reason: "admin user",
    });
  }
});

// pending delete order
router.put("/order/remove", auth.verify, (req, res) => {
  authData = auth.decode(req.headers.authorization);
  if (!authData.isAdmin) {
    userController
      .userMultiplePendingOrderRemove(authData.id, req.body)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({
      isSuccess: false,
      reason: "admin user",
    });
  }
});

router.post("/contact", (req, res) => {
  userController
    .contactUs(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

module.exports = router;
