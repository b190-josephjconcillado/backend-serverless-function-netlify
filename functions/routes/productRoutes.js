const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController.js");
const auth = require("../auth.js");

// Retrieve All Active Products
router.get("/all", (req, res) => {
  productController
    .getAllActiveProducts()
    .then((resultFromController) => res.send(resultFromController));
});
router.get("/allByAdmin", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    productController
      .getAllProductsByAdmin()
      .then((resultFromController) => res.send(resultFromController));
  }
});

router.get("/archive", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    productController
      .getAllArchiveProducts()
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({
      isSuccess: false,
      reason: "unauthorized",
    });
  }
});

// Retrieve Single Product
router.get("/:productId", (req, res) => {
  productController
    .getProduct(req.params.productId)
    .then((resultFromController) => res.send(resultFromController));
});

// Create Product (Admin Only)
router.post("/", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    productController
      .addProduct(req.body)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({
      isSuccess: false,
      reason: "unauthorized",
    });
  }
});

// Update Product Information (Admin Only)
router.put("/:productId", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    productController
      .updateProduct(req.params.productId, req.body)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({
      isSuccess: false,
      reason: "unauthorized",
    });
  }
});

// Archive Product (Admin Only)
router.put("/:productId/archive", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    productController
      .archiveProduct(req.params.productId, req.body)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({
      isSuccess: false,
      reason: "unauthorized",
    });
  }
});

module.exports = router;
