const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  firstName: {
    type: String,
    default: "",
  },
  lastName: {
    type: String,
    default: "",
  },
  mobileNumber: {
    type: String,
    default: "+63",
  },
  email: {
    type: String,
    required: [true, "Email address is required."],
  },
  password: {
    type: String,
    required: [true, "Password is required."],
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },
  profileImage: {
    folderId: {
      type: String,
      default: "",
    },
    fileId: {
      type: String,
      default: "",
    },
    link: {
      type: String,
      default:
        "https://lh3.googleusercontent.com/d/1jKpSfMOCc4aKgj_AwcR4mGPSSWXE3kXP",
    },
  },
  orders: {
    pendingOrder: {
      totalAmount: {
        type: Number,
        default: 0,
      },
      pendingOrderProducts: [
        {
          productId: {
            type: String,
            required: [true, "Product ID is required"],
          },
          quantity: {
            type: Number,
            default: 1,
          },
          name: {
            type: String,
            required: [true, "Product name is  required"],
          },
          price: {
            type: Number,
            required: [true, "Product price is required"],
          },
          transactionDate: {
            type: Date,
            default: new Date(),
          },
        },
      ],
    },
    purchasedOrder: {
      totalAmount: {
        type: Number,
        default: 0,
      },
      purchasedOrderProducts: [
        {
          productId: {
            type: String,
            required: [true, "Product ID is required"],
          },
          quantity: {
            type: Number,
            required: [true, "Product quantity is required"],
          },
          name: {
            type: String,
            required: [true, "Product name is not required"],
          },
          price: {
            type: Number,
            required: [true, "Product price is not required"],
          },
          transactionDate: {
            type: Date,
            default: new Date(),
          },
        },
      ],
    },
  },
  reset: {
    type: String,
    default: "",
  },
});

module.exports = mongoose.model("User", userSchema);
