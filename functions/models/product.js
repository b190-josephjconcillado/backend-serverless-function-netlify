const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
  productName: {
    type: String,
    required: [true, "Product name is required."],
  },
  description: {
    type: String,
    required: [true, "Description is required."],
  },
  price: {
    type: Number,
    required: [true, "Price is required"],
  },
  quantity: {
    type: Number,
    required: [true, "Quantity is required"],
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  productImage: {
    folderId: {
      type: String,
      default: "1rZ-ZBPruRNV0iwqD_9SMYfaiJGtNk72f",
    },
    fileId: {
      type: String,
      default: "",
    },
    link: {
      type: String,
      default:
        "https://lh3.googleusercontent.com/d/1clYtqGszanMrw0PRb--JGSQ3hPMaIWiB",
    },
  },
  createdOn: {
    type: Date,
    default: new Date(),
  },
});

module.exports = mongoose.model("Product", productSchema);
