const express = require("express");
const serverless = require("serverless-http");
const mongoose = require("mongoose");
const cors = require("cors");

// routes
const userRoutes = require("./routes/userRoutes.js");
const productRoutes = require("./routes/productRoutes.js");

const app = express();
// const port = 5000;

// MongoDB Connection
mongoose.connect(
  "mongodb+srv://josephjconcillado:dBOpsesYrUx0H4ib@wdc028-course-booking.i1zei0q.mongodb.net/b190-ecommerce?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("Database connected!"));

app.use(cors());

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/users", userRoutes);
app.use("/products", productRoutes);

// Server Running Confirmation
// app.listen(process.env.PORT || port, () => {
//   console.log(`API now online at port ${process.env.PORT || port}`);
// });
// app.listen(port, () => console.log(`Server running at port: ${port}`));

module.exports.handler = serverless(app);
