const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");
const { google } = require("googleapis");
const config = require("./config.js");
const User = require("./models/user.js");
const secret = "TheQuickBrownFoxJumpsOverTheLazyDog";
const bcrypt = require("bcryptjs");
const resetEmailPage = require("./resetEmailPage");
const { emailMessage } = require("./emailMessage.js");

const OAuth2 = google.auth.OAuth2;
const OAuth2_client = new OAuth2(config.clientId, config.clientSecret);
OAuth2_client.setCredentials({ refresh_token: config.refreshToken });

module.exports.supportSendMail = async (data) => {
  const accessToken = OAuth2_client.getAccessToken();

  const transport = nodemailer.createTransport({
    service: "gmail",
    auth: {
      type: "OAuth2",
      user: config.user,
      clientId: config.clientId,
      clientSecret: config.clientSecret,
      refreshToken: config.refreshToken,
      accessToken: accessToken,
    },
  });
  //   const data = {
  //     from: `"DODOFLORIE General Merchandise" <dodofloriegeneralmerchandise@gmail.com>`,
  //     to: receipient,
  //     subject: "Reset Password Link",
  //     html: `
  //             <h2>Please click on the given link below to reset you password</h2>
  //             <p>https://www.dodoflorie.ml/users/reset/${token}</p>
  //         `,
  //   };
  let info = await transport.sendMail(data);
  return info;
};

module.exports.createAccessToken = (user) => {
  const data = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin,
  };
  return jwt.sign(data, secret, {});
};

module.exports.userResetPassword = async (reqBody) => {
  const { resetToken, newPassword } = reqBody;
  return User.findOne({ reset: resetToken }).then((user) => {
    if (user === null) {
      return {
        isSuccess: false,
        reason: "user not found/token expired",
      };
    }
    let resetKey = secret + user.password;
    return jwt.verify(resetToken, resetKey, (err, data) => {
      if (err || user === null) {
        return res.send({
          isSuccess: false,
          reason: "unauthorize",
        });
      } else {
        user.password = bcrypt.hashSync(newPassword, 10);
        user.reset = "";
        return user.save().then((user, error) => {
          if (error) {
            return {
              isSuccess: false,
              reason: error,
            };
          } else {
            const data = {
              from: `"donotreply" <dodofloriegeneralmerchandise@gmail.com>`,
              to: user.email,
              subject: "Password Reset Successful",
              html: emailMessage(
                "Password Reset Successful",
                "You may now login with your new password",
                "Password Reset",
                "Go back to homepage",
                "https://www.dodoflorie.ml/"
              ),
            };
            this.supportSendMail(data);

            return {
              isSuccess: true,
              reason: "password reset successful",
            };
          }
        });
      }
    });
  });
};

module.exports.userForgotPassword = async (reqBody) => {
  return User.findOne({ email: reqBody.email }).then((result) => {
    if (result === null) {
      return {
        isSuccess: false,
        reason: "user not exists",
        user: result,
      };
    } else {
      const resetKey = secret + result.password;
      const token = jwt.sign({ id: result._id }, resetKey, {
        expiresIn: "15m",
      });

      const accessToken = OAuth2_client.getAccessToken();

      const transport = nodemailer.createTransport({
        service: "gmail",
        auth: {
          type: "OAuth2",
          user: config.user,
          clientId: config.clientId,
          clientSecret: config.clientSecret,
          refreshToken: config.refreshToken,
          accessToken: accessToken,
        },
      });
      const data = {
        from: `"donotreply" <dodofloriegeneralmerchandise@gmail.com>`,
        to: reqBody.email,
        subject: "Reset Password Link",
        html: resetEmailPage.emailResetMessage(
          `https://www.dodoflorie.ml/reset/${token}`
        ),
      };
      return User.findByIdAndUpdate(result._id, { reset: token }).then(
        async (resultUpdate) => {
          let info = await transport.sendMail(data);
          return info;
        }
      );
    }
  });
};

// Token Verification
module.exports.verify = (req, res, next) => {
  let token = req.headers.authorization;
  if (typeof token !== "undefined") {
    // the token sent is a type of "Bearer" token which when received, contains the word "Bearer" as a prefix to the string
    token = token.slice(7, token.length);
    // verify() - validates the token decrypting the token using the secret code
    return jwt.verify(token, secret, (err, data) => {
      if (err) {
        return res.send({
          isSuccess: false,
          reason: "unauthorize",
        });
      } else {
        next();
      }
    });
  } else {
    return res.send({
      isSuccess: false,
      reason: "unauthorize",
    });
  }
};

// Token decryption
module.exports.decode = (token) => {
  if (typeof token !== "undefined") {
    token = token.slice(7, token.length);
    return jwt.verify(token, secret, (err, data) => {
      if (err) {
        return null;
      } else {
        return jwt.decode(token, { complete: true }).payload;
      }
    });
  } else {
    return null;
  }
};
