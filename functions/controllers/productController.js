const Product = require("../models/product.js");
const mongoose = require("mongoose");
const { search } = require("../routes/userRoutes.js");

// Retrieve All Active Products
module.exports.getAllActiveProducts = () => {
  return Product.find({ isActive: true }).then((result) => {
    if (result == null || result == "") {
      return {
        isSuccess: false,
        reason: "product not found",
      };
    } else {
      return result;
    }
  });
};
module.exports.getAllProductsByAdmin = () => {
  return Product.find({}).then((result) => {
    if (result == null || result == "") {
      return {
        isSuccess: false,
        reason: "product not found",
      };
    } else {
      return result;
    }
  });
};

module.exports.getAllArchiveProducts = () => {
  return Product.find({ isActive: false }).then((result) => {
    if (result == null || result == "") {
      return {
        isSuccess: false,
        reason: "archived product not found",
      };
    } else {
      return result;
    }
  });
};

// Retrieve Single Product
module.exports.getProduct = (productId) => {
  productId = mongoose.Types.ObjectId(productId);
  return Product.findById(productId).then((result, error) => {
    if (error || result === null) {
      return {
        isSuccess: false,
        reason: "server error - product not found",
      };
    } else {
      if (result.isActive) {
        return result;
      } else {
        return {
          isSuccess: false,
          reason: "out of stock",
        };
      }
    }
  });
};

// Create Product (Admin Only)
// module.exports.addProduct = (reqBody) => {
//     return Product.findOne({productName: reqBody.productName}).then(result => {
//         if(result === null) {
//             let newProduct = new Product({
//                 productName: reqBody.productName,
//                 description: reqBody.description,
//                 price: reqBody.price,
//                 quantity: reqBody.quantity
//             });
//             return newProduct.save().then((product,error) => {
//                 if(error) {
//                     return {
//                         isSuccess: false,
//                         reason: error
//                     };
//                 } else {
//                     return {
//                         isSuccess: true,
//                         reason: "added",
//                         product: product
//                     };
//                 }
//             })
//         } else {
//             return {
//                 isSuccess: false,
//                 reason: "product exists"
//             };
//         }
//     });
// };

module.exports.addProduct = (reqBody) => {
  return Product.find({}).then((result) => {
    let searchIndex = result.findIndex(
      (product) => product.productName == reqBody.productName
    );
    if (searchIndex > -1) {
      return {
        isSuccess: false,
        reason: "product exists",
        productName: result.productName,
      };
    } else {
      let newProduct = new Product({
        productName: reqBody.productName,
        description: reqBody.description,
        price: reqBody.price,
        quantity: reqBody.quantity,
        productImage: {
          folderId: reqBody.folderId,
          fileId: reqBody.fileId,
          link: reqBody.link,
        },
      });
      return newProduct.save().then((product, error) => {
        if (error) {
          console.log(error);
        } else {
          return {
            isSuccess: true,
            result: product,
          };
        }
      });
    }
  });
};

// Update Product Information (Admin Only)
module.exports.updateProduct = (productId, reqBody) => {
  productId = mongoose.Types.ObjectId(productId);
  let updateProduct = {
    productName: reqBody.productName,
    description: reqBody.description,
    price: reqBody.price,
    quantity: reqBody.quantity,
    isActive: reqBody.isActive,
    productImage: {
      folderId: reqBody.folderId,
      fileId: reqBody.fileId,
      link: reqBody.link,
    },
  };

  return Product.findByIdAndUpdate(productId, updateProduct).then(
    (result, error) => {
      if (error || result === null) {
        return {
          isSuccess: false,
          reason: "process failed - product not found",
        };
      } else {
        return {
          isSuccess: true,
          reason: "product updated",
          product: result.ProductName,
        };
      }
    }
  );
};

// Archive Product (Admin Only)
module.exports.archiveProduct = (productId, reqBody) => {
  productId = mongoose.Types.ObjectId(productId);
  let archiveProduct = {
    isActive: reqBody.isActive,
  };
  return Product.findByIdAndUpdate(productId, archiveProduct).then(
    (result, error) => {
      if (error || result === null) {
        return {
          isSuccess: false,
          reason: "process failed - product not found",
        };
      } else {
        if (result.isActive) {
          return {
            isSuccess: true,
            reason: "archived",
            product: {
              productId: result._id,
              productName: result.productName,
              isActive: false,
            },
          };
        } else {
          return {
            isSuccess: false,
            reason: "already archived",
            productId: result._id,
          };
        }
      }
    }
  );
};
