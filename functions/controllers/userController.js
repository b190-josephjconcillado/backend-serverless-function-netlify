const User = require("../models/user.js");
const bcrypt = require("bcryptjs");
const auth = require("../auth.js");
const Product = require("../models/product.js");
const mongoose = require("mongoose");
const product = require("../models/product.js");
const emailPage = require("../emailPage");
const emailMessage = require("../emailMessage");

module.exports.checkUser = (reqBody) => {
  return User.findOne({ email: reqBody.email }).then((result) => {
    if (result !== null) {
      return {
        isSuccess: false,
        reason: "Email found",
      };
    } else {
      return {
        isSuccess: true,
        reason: "Email not found",
      };
    }
  });
};

// User Registration
module.exports.registerUser = (reqBody) => {
  return User.findOne({ email: reqBody.email }).then((result) => {
    if (result === null) {
      let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        mobileNumber: reqBody.mobileNumber,
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10),
        profileImage: {
          folderId: reqBody.folderId,
        },
      });

      return newUser.save().then((user, error) => {
        if (error) {
          return {
            isSuccess: false,
            reason: error,
            user: user.email,
          };
        } else {
          const data = {
            from: `"donotreply" <dodofloriegenralmerchandise@gmail.com>`,
            to: reqBody.email,
            subject: "Welcome to DODOFLORIE General Merchandise",
            html: emailPage.emailWelcomeMessage(),
          };
          auth.supportSendMail(data);

          return {
            isSuccess: true,
            reason: "user successfully registered",
            user: user.email,
            userId: user._id,
          };
        }
      });
    } else {
      return {
        isSuccess: false,
        reason: "user exists",
        user: result.email,
      };
    }
  });
};

module.exports.uploadImageProfile = async (userId, data) => {
  let links = {
    profileImage: {
      links: [],
    },
  };
  return await fetch(
    "https://script.google.com/macros/s/AKfycbzihmSAX7bJhbfkVsqd3bvlP0jTe3TEbsOcVRzvLdIC2KtJYOXXWptFEHidcf_jUVpZ/exec",
    {
      method: "POST",
      body: JSON.stringify(data),
    }
  )
    .then((res) => res.json())
    .then(async (a) => {
      links.profileImage.links.push(a.url);
      const result = await User.findByIdAndUpdate(userId, links);
      return {
        isSuccess: true,
        reason: "user successfully uploaded image",
        imageUrl: a.url,
        result: a,
      };
    });
};

module.exports.userChangePassword = (userId, reqBody) => {
  return User.findOne({ _id: userId }).then((result) => {
    if (result === null) {
      return {
        isSuccess: false,
        reason: "user not exists",
        user: result,
      };
    } else {
      const isPasswordCorrect = bcrypt.compareSync(
        reqBody.password,
        result.password
      );
      if (isPasswordCorrect) {
        result.password = bcrypt.hashSync(reqBody.newPassword, 10);
        return result.save().then((user, error) => {
          if (error) {
            return {
              isSuccess: false,
              reason: error,
              user: user.email,
            };
          } else {
            return {
              isSuccess: true,
              reason: "password updated",
              user: user.email,
              userId: user._id,
            };
          }
        });
      } else {
        return {
          isSuccess: false,
          reason: "incorrect password",
        };
      }
    }
  });
};

// User Login
module.exports.loginUser = (reqBody) => {
  return User.findOne({ email: reqBody.email }).then((result) => {
    if (result === null) {
      return {
        isSuccess: false,
        reason: "user not exists",
        user: result,
      };
    } else {
      const isPasswordCorrect = bcrypt.compareSync(
        reqBody.password,
        result.password
      );
      if (isPasswordCorrect) {
        const sum = result.orders.pendingOrder.pendingOrderProducts.reduce(
          (acc, ob) => {
            return acc + ob.quantity;
          },
          0
        );
        return {
          isSuccess: true,
          access: auth.createAccessToken(result),
          userId: result._id,
          firstName: result.firstName,
          lastName: result.lastName,
          mobileNumber: result.mobileNumber,
          email: result.email,
          folderId: result.folderId,
          fileId: result.fileId,
          profileImage: result.profileImage,
          isAdmin: result.isAdmin,
          cart: sum,
        };
      } else {
        return {
          isSuccess: false,
          reason: "auth failed",
          user: result.email,
        };
      }
    }
  });
};

// User profile
module.exports.getUserProfile = (userId) => {
  return User.findById(userId).then((user) => {
    if (user === null) {
      return {
        isSuccess: false,
      };
    } else {
      const sum = user.orders.pendingOrder.pendingOrderProducts.reduce(
        (acc, ob) => {
          return acc + ob.quantity;
        },
        0
      );

      return {
        userId: user._id,
        firstName: user.firstName,
        lastName: user.lastName,
        mobileNumber: user.mobileNumber,
        email: user.email,
        folderId: user.profileImage.folderId,
        fileId: user.profileImage.fileId,
        profileImage: user.profileImage.link,
        isAdmin: user.isAdmin,
        cart: sum,
      };
    }
  });
};

// Admin Only Get All User Details
module.exports.getAllUserDetails = () => {
  return User.find({}).then((result) => {
    if (result === null) {
      return {
        isSuccess: false,
      };
    } else {
      if (result.length > 0) {
        for (i = 0; i < result.length; i++) {
          if (!result[i].isAdmin) {
            result[i] = {
              userId: result[i]._id,
              firstName: result[i].firstName,
              lastName: result[i].lastName,
              mobileNumber: result[i].mobileNumber,
              email: result[i].email,
              isAdmin: result[i].isAdmin,
              profileImage: result[i].profileImage.link,
              orders: {
                pendingOrder: {
                  totalAmount: result[i].orders.pendingOrder.totalAmount,
                  products:
                    result[i].orders.pendingOrder.pendingOrderProducts.length,
                },
                purchasedOrder: {
                  totalAmount: result[i].orders.purchasedOrder.totalAmount,
                  products:
                    result[i].orders.purchasedOrder.purchasedOrderProducts
                      .length,
                },
              },
            };
          } else {
            result[i] = {
              userId: result[i]._id,
              firstName: result[i].firstName,
              lastName: result[i].lastName,
              mobileNumber: result[i].mobileNumber,
              email: result[i].email,
              isAdmin: result[i].isAdmin,
              profileImage: result[i].profileImage.link,
            };
          }
        }
      }
      return {
        isSuccess: true,
        result: result,
      };
    }
  });
};

// Update User Profile
module.exports.updateUserProfile = (userId, reqBody) => {
  let updateUser = {
    firstName: reqBody.firstName,
    lastName: reqBody.lastName,
    email: reqBody.email,
    mobileNumber: reqBody.mobileNumber,
    profileImage: {
      folderId: reqBody.folderId,
      fileId: reqBody.fileId,
      link: reqBody.profileImage,
    },
  };
  return User.findByIdAndUpdate(userId, updateUser).then((result, error) => {
    if (error) {
      return {
        isSuccess: false,
        reason: "server error",
      };
    } else {
      return {
        isSuccess: true,
        reason: "successfully updated",
        userId: result._id,
      };
    }
  });
};

// Set User As Admin
module.exports.userAsAdmin = (userId, reqBody) => {
  let admin = {
    isAdmin: reqBody.isAdmin,
  };
  return User.findByIdAndUpdate(userId, admin).then((result, error) => {
    if (error) {
      return {
        isSuccess: false,
        reason: "update failed",
      };
    } else {
      return {
        isSuccess: true,
        reason: "user promoted",
        user: result.email,
      };
    }
  });
};

// Non-admin Create Pending order and Checkout order
module.exports.userOrder = (productId, qty, userId) => {
  qty = parseInt(qty);
  return Product.findById(productId).then((product) => {
    if (product !== null && product.isActive) {
      if (product.quantity < qty) qty = product.quantity;
      else if (qty === null) qty = 1;

      return User.findById(userId).then((user) => {
        if (user !== null) {
          user.orders.pendingOrder.totalAmount =
            user.orders.pendingOrder.totalAmount + product.price * qty;

          if (user.orders.pendingOrder.pendingOrderProducts.length > 0) {
            let idx = user.orders.pendingOrder.pendingOrderProducts.findIndex(
              (value) => value.productId === productId
            );
            if (idx >= 0) {
              user.orders.pendingOrder.pendingOrderProducts[idx].quantity =
                user.orders.pendingOrder.pendingOrderProducts[idx].quantity +
                qty;
              user.orders.pendingOrder.pendingOrderProducts[
                idx
              ].transactionDate = new Date();
            } else {
              user.orders.pendingOrder.pendingOrderProducts.push({
                productId: productId,
                quantity: qty,
                name: product.productName,
                price: product.price,
              });
            }
          } else {
            user.orders.pendingOrder.pendingOrderProducts.push({
              productId: productId,
              quantity: qty,
              name: product.productName,
              price: product.price,
            });
          }

          return user.save().then((result, error) => {
            if (error) {
              return {
                isSuccess: false,
                reason: "order failed - server error",
              };
            } else {
              let updateProduct;
              qty = product.quantity - qty;
              if (qty === 0) {
                updateProduct = {
                  quantity: qty,
                  isActive: false,
                };
              } else {
                updateProduct = {
                  quantity: qty,
                };
              }
              return Product.findByIdAndUpdate(product._id, updateProduct).then(
                (result, error) => {
                  if (error) {
                    return {
                      isSuccess: false,
                      reason: "order failed - server error",
                    };
                  } else {
                    return {
                      isSuccess: true,
                      reason: "order placed",
                    };
                  }
                }
              );
            }
          });
        } else {
          return {
            isSuccess: false,
            reason: "invalid user",
          };
        }
      });
    } else {
      return {
        isSuccess: false,
        reason: "out of stock",
      };
    }
  });
};

module.exports.userCheckout = (userId) => {
  return User.findById(userId).then((user) => {
    if (user.orders.pendingOrder.totalAmount !== 0) {
      user.orders.purchasedOrder.totalAmount =
        user.orders.purchasedOrder.totalAmount +
        user.orders.pendingOrder.totalAmount;

      let totalAmount = user.orders.pendingOrder.totalAmount;
      user.orders.pendingOrder.totalAmount = 0;
      for (
        i = 0;
        i < user.orders.pendingOrder.pendingOrderProducts.length;
        i++
      ) {
        user.orders.pendingOrder.pendingOrderProducts[i].transactionDate =
          new Date();
        user.orders.purchasedOrder.purchasedOrderProducts.push(
          user.orders.pendingOrder.pendingOrderProducts[i]
        );
      }
      user.orders.pendingOrder.pendingOrderProducts = [];
      return user.save().then((result, error) => {
        if (error) {
          return {
            isSuccess: false,
            reason: "order checkout failed - server error",
          };
        } else {
          return {
            isSuccess: true,
            reason: "order successfully paid",
            totalAmount: totalAmount,
          };
        }
      });
    } else {
      return {
        isSuccess: false,
        reason: "no orders yet",
      };
    }
  });
};

// Retrieve authenticated user's orders & retrieve all orders (Admin only)
// Users
module.exports.userGetPendingOrder = (userData) => {
  if (!userData.isAdmin) {
    return User.findById(userData.id).then((user) => {
      if (user.orders.pendingOrder.totalAmount !== 0) {
        return {
          isSuccess: true,
          userId: userData.id,
          email: user.email,
          pendingOrder: user.orders.pendingOrder,
        };
      } else {
        return {
          isSuccess: false,
          reason: "no orders yet",
        };
      }
    });
  } else {
    return User.find({}).then((user) => {
      let allUserPendingOrders = [];
      for (i = 0; i < user.length; i++) {
        if (!user[i].isAdmin) {
          allUserPendingOrders.push({
            isSuccess: true,
            userId: user[i]._id,
            email: user[i].email,
            pendingOrders: user[i].orders.pendingOrder,
          });
        }
      }
      return allUserPendingOrders;
    });
  }
};
module.exports.userGetPurchasedOrder = (userData) => {
  if (!userData.isAdmin) {
    return User.findById(userData.id).then((user) => {
      if (user.orders.purchasedOrder.totalAmount !== 0) {
        return {
          isSuccess: true,
          userId: userData.id,
          email: user.email,
          purchasedOrder: user.orders.purchasedOrder,
        };
      } else {
        return {
          isSuccess: false,
          reason: "no purchased order yet",
        };
      }
    });
  } else {
    return User.find({}).then((user) => {
      let allUserPurchasedOrders = [];
      for (i = 0; i < user.length; i++) {
        if (!user[i].isAdmin) {
          allUserPurchasedOrders.push({
            isSuccess: true,
            userId: user[i]._id,
            email: user[i].email,
            purchasedOrders: user[i].orders.purchasedOrder,
          });
        }
      }
      return allUserPurchasedOrders;
    });
  }
};

// additional multiple order
// Non-admin Create Pending order and Checkout order
module.exports.userMultipleOrder = (userId, reqBody) => {
  let finalResult = [];
  return User.findById(userId).then((user, error) => {
    let pendingOrderList = user.orders.pendingOrder.pendingOrderProducts;
    if (error) {
      return {
        isSuccess: false,
        reason: "server error",
      };
    } else {
      return Product.find({}).then((product) => {
        for (i = 0; i < product.length; i++) {
          for (x = 0; x < reqBody.length; x++) {
            if (product[i]._id == reqBody[x].productId && product[i].isActive) {
              if (product[i].quantity < reqBody[x].quantity)
                reqBody[x].quantity = product[i].quantity;

              let searchIndex =
                user.orders.pendingOrder.pendingOrderProducts.findIndex(
                  (product) => product.productId == reqBody[x].productId
                );

              if (searchIndex > -1) {
                user.orders.pendingOrder.pendingOrderProducts[
                  searchIndex
                ].quantity =
                  pendingOrderList[searchIndex].quantity + reqBody[x].quantity;
                finalResult.push({
                  isSuccess: true,
                  reason: "order placed",
                  productId: reqBody[x].productId,
                });
              } else {
                user.orders.pendingOrder.pendingOrderProducts.push({
                  productId: reqBody[x].productId,
                  quantity: reqBody[x].quantity,
                });
                finalResult.push({
                  isSuccess: true,
                  reason: "order placed",
                  productId: reqBody[x].productId,
                });
              }

              user.orders.pendingOrder.totalAmount =
                user.orders.pendingOrder.totalAmount +
                product[i].price * reqBody[x].quantity;

              let updateProduct;
              let qty = product[i].quantity - reqBody[x].quantity;
              if (qty == 0) {
                updateProduct = {
                  quantity: qty,
                  isActive: false,
                };
              } else {
                updateProduct = {
                  quantity: qty,
                };
              }
              Product.findByIdAndUpdate(
                reqBody[x].productId,
                updateProduct
              ).then((result, error) => {
                if (error) {
                  return {
                    isSuccess: false,
                    reason: "server error",
                  };
                }
              });
            } else if (
              product[i]._id == reqBody[x].productId &&
              !product[i].isActive
            ) {
              finalResult.push({
                isSuccess: false,
                reason: "out of stock",
                productId: reqBody[x].productId,
              });
            }
          }
        }
        return user.save().then((result, error) => {
          if (error) {
            return {
              isSuccess: false,
              reason: "server error",
            };
          } else {
            return {
              orders: finalResult,
            };
          }
        });
      });
    }
  });
};

//Pending remove order
module.exports.userMultiplePendingOrderRemove = (userId, reqBody) => {
  let finalResult = [];
  return User.findById(userId).then((user, error) => {
    let pendingOrderList = user.orders.pendingOrder.pendingOrderProducts;
    if (error) {
      return {
        isSuccess: false,
        reason: "server error",
      };
    } else {
      return Product.find({}).then((product) => {
        for (i = 0; i < product.length; i++) {
          for (x = 0; x < reqBody.length; x++) {
            if (product[i]._id == reqBody[x].productId) {
              let searchIndex =
                user.orders.pendingOrder.pendingOrderProducts.findIndex(
                  (product) => product.productId == reqBody[x].productId
                );
              if (searchIndex > -1) {
                if (
                  user.orders.pendingOrder.pendingOrderProducts[searchIndex]
                    .quantity < reqBody[x].quantity
                )
                  reqBody[x].quantity =
                    user.orders.pendingOrder.pendingOrderProducts[
                      searchIndex
                    ].quantity;
                user.orders.pendingOrder.pendingOrderProducts[
                  searchIndex
                ].quantity =
                  pendingOrderList[searchIndex].quantity - reqBody[x].quantity;
                finalResult.push({
                  isSuccess: true,
                  reason: "order removed",
                  quantity: reqBody[x].quantity,
                  productId: reqBody[x].productId,
                });

                if (
                  user.orders.pendingOrder.pendingOrderProducts[searchIndex]
                    .quantity == 0
                ) {
                  user.orders.pendingOrder.pendingOrderProducts.splice(
                    searchIndex,
                    1
                  );
                }
              } else {
                reqBody[x].quantity = 0;
                finalResult.push({
                  isSuccess: false,
                  reason: "productId not found",
                });
              }

              user.orders.pendingOrder.totalAmount =
                user.orders.pendingOrder.totalAmount -
                product[i].price * reqBody[x].quantity;

              let updateProduct;
              let qty = product[i].quantity + reqBody[x].quantity;
              if (product[i].quantity == 0 && qty > 0) {
                updateProduct = {
                  quantity: qty,
                  isActive: true,
                };
              } else {
                updateProduct = {
                  quantity: qty,
                };
              }
              Product.findByIdAndUpdate(
                reqBody[x].productId,
                updateProduct
              ).then((result, error) => {
                if (error) {
                  return {
                    isSuccess: false,
                    reason: "server error",
                  };
                }
              });
            }
          }
        }
        return user.save().then((result, error) => {
          if (error) {
            return {
              isSuccess: false,
              reason: "server error",
            };
          } else {
            return {
              orders: finalResult,
            };
          }
        });
      });
    }
  });
};

module.exports.userSinglePendingOrderRemove = async (
  userId,
  reqQty,
  productId
) => {
  reqQty = parseInt(reqQty);
  let finalResult = [];
  return User.findById(userId).then((user, error) => {
    let pendingOrderList = user.orders.pendingOrder.pendingOrderProducts;
    if (error) {
      return {
        isSuccess: false,
        reason: "server error",
      };
    } else {
      return Product.findById(productId).then((result, error) => {
        if (error || result == null || result == "") {
          return {
            isSuccess: false,
            reason: "server error",
          };
        } else {
          let searchIndex =
            user.orders.pendingOrder.pendingOrderProducts.findIndex(
              (product) => product.productId == productId
            );
          if (searchIndex > -1) {
            if (
              user.orders.pendingOrder.pendingOrderProducts[searchIndex]
                .quantity < reqQty
            )
              reqQty =
                user.orders.pendingOrder.pendingOrderProducts[searchIndex]
                  .quantity;

            user.orders.pendingOrder.pendingOrderProducts[
              searchIndex
            ].quantity =
              user.orders.pendingOrder.pendingOrderProducts[searchIndex]
                .quantity - reqQty;
            if (
              user.orders.pendingOrder.pendingOrderProducts[searchIndex]
                .quantity <= 0
            ) {
              user.orders.pendingOrder.pendingOrderProducts.splice(
                searchIndex,
                1
              );
            }

            user.orders.pendingOrder.totalAmount =
              user.orders.pendingOrder.totalAmount - result.price * reqQty;

            let updateProduct;
            let qty = result.quantity + reqQty;

            if (result.quantity === 0 && qty > 0) {
              updateProduct = {
                quantity: qty,
                isActive: true,
              };
            } else {
              updateProduct = {
                quantity: qty,
              };
            }

            return Product.findByIdAndUpdate(productId, updateProduct).then(
              (result, error) => {
                if (error) {
                  return {
                    isSuccess: false,
                    reason: "server error",
                  };
                } else {
                  return user.save().then((result, error) => {
                    if (error) {
                      return {
                        isSuccess: false,
                        reason: "server error",
                      };
                    } else {
                      return {
                        isSuccess: true,
                        reason: "order removed",
                        quantity: reqQty,
                        productId: result._id,
                      };
                    }
                  });
                }
              }
            );
          } else {
            return {
              isSuccess: false,
              reason: "productId not found",
            };
          }
        }
      });
    }
  });
};

module.exports.contactUs = async (reqBody) => {
  const data = {
    from: `"donotreply" <dodofloriegeneralmerchandise@gmail.com>`,
    to: reqBody.email,
    subject: "Thank You For Contacting Us!",
    html: emailMessage.emailMessage(
      `Hi ${reqBody.name}.`,
      "Thank you for contacting us",
      reqBody.message,
      `GO BACK TO WEBSITE`,
      `https://www.dodoflorie.ml/`
    ),
  };

  return await auth.supportSendMail(data);
};
